/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ascend.o2o.web.rest.vm;
