package com.ascend.o2o.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ascend.o2o.domain.Merchant;
import com.ascend.o2o.domain.*; // for static metamodels
import com.ascend.o2o.repository.MerchantRepository;
import com.ascend.o2o.service.dto.MerchantCriteria;


/**
 * Service for executing complex queries for Merchant entities in the database.
 * The main input is a {@link MerchantCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Merchant} or a {@link Page} of {@link Merchant} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MerchantQueryService extends QueryService<Merchant> {

    private final Logger log = LoggerFactory.getLogger(MerchantQueryService.class);


    private final MerchantRepository merchantRepository;

    public MerchantQueryService(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    /**
     * Return a {@link List} of {@link Merchant} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Merchant> findByCriteria(MerchantCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Merchant> specification = createSpecification(criteria);
        return merchantRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Merchant} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Merchant> findByCriteria(MerchantCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Merchant> specification = createSpecification(criteria);
        return merchantRepository.findAll(specification, page);
    }

    /**
     * Function to convert MerchantCriteria to a {@link Specifications}
     */
    private Specifications<Merchant> createSpecification(MerchantCriteria criteria) {
        Specifications<Merchant> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Merchant_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Merchant_.name));
            }
        }
        return specification;
    }

}
