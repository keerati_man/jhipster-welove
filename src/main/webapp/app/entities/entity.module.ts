import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { WeloveMerchantModule } from './merchant/merchant.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        WeloveMerchantModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WeloveEntityModule {}
