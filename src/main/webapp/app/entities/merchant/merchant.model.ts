import { BaseEntity } from './../../shared';

export class Merchant implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public logoContentType?: string,
        public logo?: any,
    ) {
    }
}
