import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MerchantComponent } from './merchant.component';
import { MerchantDetailComponent } from './merchant-detail.component';
import { MerchantPopupComponent } from './merchant-dialog.component';
import { MerchantDeletePopupComponent } from './merchant-delete-dialog.component';

@Injectable()
export class MerchantResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const merchantRoute: Routes = [
    {
        path: 'merchant',
        component: MerchantComponent,
        resolve: {
            'pagingParams': MerchantResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'weloveApp.merchant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'merchant/:id',
        component: MerchantDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'weloveApp.merchant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const merchantPopupRoute: Routes = [
    {
        path: 'merchant-new',
        component: MerchantPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'weloveApp.merchant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'merchant/:id/edit',
        component: MerchantPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'weloveApp.merchant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'merchant/:id/delete',
        component: MerchantDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'weloveApp.merchant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
